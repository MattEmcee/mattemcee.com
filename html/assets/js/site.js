// initiate the lightbx with contact form	
function send_a_message() {
	$('#lightbox').show('fade', 1000, function() {
		$('#lightbox-shadow').show('fade', 1000);
	});
};

// Validate form fields
function validate_text() {
	var name_val = $('#Name').val();
	var message_val = $('#Message').val();
	
	if (name_val == null || name_val.length <= 2)
	{
		$('#Name').addClass('invalid').attr('placeholder', 'Minimum 3 characters.');
		if (name_val.length == 0)
		{
			alert('You have no name?? \nI\'d like to get back to you, but you haven\'t given me enough info! \nERROR: no name provided.');
		};
		if (name_val.length > 0 && name_val.length <=2)
		{
			alert(name_val + '?? Is that really your name? \nI\'d like to get back to you, but you haven\'t given me enough info! \nERROR: name is too short [' + name_val.length + '. Min 3 char.');
		};	
		return false;
	};
	if (message_val == '' || message_val.length <= 10) 
	{
		$('#Message').addClass('invalid').attr('placeholder', 'This field is required. Minimum 10 characters');
		if (message_val.length == 0)
		{
			alert('I\'d like to get back to you, but you haven\'t given me enough info! \nERROR: no message provided.');
		};
		if (message_val.length > 0 && message_val.length <= 10)
		{
			alert('I\'d like to get back to you, but you haven\'t given me enough info! \nERROR: message is too short [' + message_val.length + ']. Min 10 char.');
		};
		return false;	
	};
	return true;
};

function validate_email() {
	var email_val = $('[name="email_address"]').val();
	var atPos = email_val.indexOf("@");
	var dotPos = email_val.lastIndexOf(".");
	
	if (atPos < 1 || dotPos < atPos + 2 || dotPos + 2 >= email_val.length)
	{
		$('#Email').addClass('invalid').attr('placeholder', 'Example: a_username@domain.ext');
		if (email_val.length == 0)
		{
			alert('I\'d like to get back to you, but you haven\'t given me enough info! \nERROR: no email address provided.');
		};
		if (email_val.length > 0 && atPos < 1)
		{
			alert('Ah! That\'s not a valid email address! \nERROR: invalid structure. Null or invalid @ placement -- interpreting: ' + email_val);
		};
		if (dotPos < atPos + 2 || dotPos + 2 >= email_val.length)
		{
			alert('Ah! That\'s not a valid email address! \nERROR: email address is too short [' + email_val.length + '] or invalid domain.');
		};
		return false;
	};
	return true;
};

function validate() {
	if (validate_text() == false) return false;
	if (validate_email() == false) return false;
	return true;
};
	
// Document Ready
$(document).ready(function () {
	
	// click to close the lightbox
	$('#lightbox-shadow').click(function() {
		$('#lightbox').hide('fade', 500);
		$('#lightbox-shadow').hide('fade', 500);
	});
	
	// field invalid listeners
	$(':input, :focus').change(function () {
		var input_val = $(this).val();
		console.log(input_val);
		
		if (input_val.length <= 2 || input_val == '')
		{
			$(this).addClass('invalid');
		}
		else
		{
			$(this).removeClass('invalid');	
		};
	});
});