<?php

// CHANGE THE VARIABLES BELOW

$EmailFrom = "serveradmin@mattemcee.com";
$EmailTo = "matt@mattemcee.com";
$Subject = "From: mattemcee.com";

$Name = Trim(stripslashes($_POST['Name'])); 
$Email = Trim(stripslashes($_POST['Email'])); 
$Message = Trim(stripslashes($_POST['Message'])); 

// prepare email body text
$Body = "";
$Body .= "Name: ";
$Body .= $Name;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n";
$Body .= "Notes: ";
$Body .= $Message;
$Body .= "\n";

// send email 
$success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");

// redirect to success page
// CHANGE THE URL BELOW TO YOUR "THANK YOU" PAGE
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=contact_engine/contact_submit.html\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=contact_engine/contact_error.html\">";
}
?>